using System.Collections.Generic;

namespace pwt_0x01.Models
{
    public class CarouselViewModel
    {
        public IList<Carousel> Carousels { get; set; }
    }
}
