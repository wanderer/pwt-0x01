namespace pwt_0x01.Models
{
    public class Carousel
    {
        public string DataTarget { get; set; }
        public string ImageSrc { get; set; }
        public string ImageAlt { get; set; }
        public string CarouselContent { get; set; }
    }
}
