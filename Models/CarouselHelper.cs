using System.Collections.Generic;

namespace pwt_0x01.Models
{
    public class CarouselHelper
    {
        public static IList<Carousel> GenerateCarousel()
        {
            IList<Carousel> carousel = new List<Carousel>()
            {
                new Carousel() {DataTarget = "#myCarousel", ImageSrc = "/images/banner1.svg", ImageAlt = "img1", CarouselContent = "Learn how to build ASP.NET apps that can run anywhere. <a class=\"btn btn-default\" href=\"https://go.microsoft.com/fwlink/?LinkID=525028&clcid=0x409\">Learn More</a>"},
                new Carousel() {DataTarget = "#myCarousel", ImageSrc = "/images/banner2.svg", ImageAlt = "img2", CarouselContent = "There are powerful new features in Visual Studio for building modern web apps. <a class=\"btn btn-default\" href=\"https://go.microsoft.com/fwlink/?LinkID=525030&clcid=0x409\">Learn More</a>"},
                new Carousel() {DataTarget = "#myCarousel", ImageSrc = "/images/banner3.svg", ImageAlt = "img3", CarouselContent = "Learn how Microsoft's Azure cloud platform allows you to build, deploy, and scale web apps.<a class=\"btn btn-default\" href=\"https://go.microsoft.com/fwlink/?LinkID=525027&clcid=0x409\">Learn More</a>"},
                new Carousel() {DataTarget = "#myCarousel", ImageSrc = "/images/ms_loves_linux.jpeg", ImageAlt = "ms loves linux", CarouselContent = "m$ loves linux, reportedly<a class=\"btn btn-default\" href=\"#\">Learn More</a>"}
            };
            return carousel;
        }
    }
}
