﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pwt_0x01.Models;

namespace pwt_0x01.Controllers
{
    public class HomeController : Controller
    {
        private IList<Carousel> Carousels = CarouselHelper.GenerateCarousel();

        public IActionResult Index()
        {
            CarouselViewModel carousel = new CarouselViewModel();
            carousel.Carousels = Carousels;
            return View(carousel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}
